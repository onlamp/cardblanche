--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: offers_rubrics; Type: TABLE; Schema: public; Owner: cardblanche; Tablespace: 
--

CREATE TABLE offers_rubrics (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    url character varying(255) NOT NULL
);


ALTER TABLE public.offers_rubrics OWNER TO cardblanche;

--
-- Data for Name: offers_rubrics; Type: TABLE DATA; Schema: public; Owner: cardblanche
--

INSERT INTO offers_rubrics VALUES (3, 'Красота и здоровье', 'beauty-and-health');
INSERT INTO offers_rubrics VALUES (5, 'Авто', 'avto');
INSERT INTO offers_rubrics VALUES (6, 'Развлечения', 'entertainment');
INSERT INTO offers_rubrics VALUES (4, 'Туризм', 'tourism');
INSERT INTO offers_rubrics VALUES (9, 'Спорт', 'sport');
INSERT INTO offers_rubrics VALUES (10, 'Кафе и рестораны', 'cafes-and-restaurants');
INSERT INTO offers_rubrics VALUES (13, 'Курсы и тренинги', 'courses');
INSERT INTO offers_rubrics VALUES (15, 'Магазины', 'shops');
INSERT INTO offers_rubrics VALUES (16, 'Услуги', 'service');


--
-- Name: offers_rubrics_pkey; Type: CONSTRAINT; Schema: public; Owner: cardblanche; Tablespace: 
--

ALTER TABLE ONLY offers_rubrics
    ADD CONSTRAINT offers_rubrics_pkey PRIMARY KEY (id);


--
-- Name: uniq_369a06d15e237e06; Type: INDEX; Schema: public; Owner: cardblanche; Tablespace: 
--

CREATE UNIQUE INDEX uniq_369a06d15e237e06 ON offers_rubrics USING btree (name);


--
-- Name: uniq_369a06d1f47645ae; Type: INDEX; Schema: public; Owner: cardblanche; Tablespace: 
--

CREATE UNIQUE INDEX uniq_369a06d1f47645ae ON offers_rubrics USING btree (url);


--
-- PostgreSQL database dump complete
--

