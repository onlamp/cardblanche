--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cities; Type: TABLE; Schema: public; Owner: cardblanche; Tablespace: 
--

CREATE TABLE cities (
    id integer NOT NULL,
    country_id integer NOT NULL,
    name character varying(255) NOT NULL,
    defaults boolean DEFAULT false NOT NULL,
    stick boolean DEFAULT false NOT NULL,
    seo_name character varying(255) DEFAULT NULL::character varying,
    "position" character varying(255) DEFAULT NULL::character varying,
    show_partner boolean DEFAULT false NOT NULL
);


ALTER TABLE public.cities OWNER TO cardblanche;

--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: cardblanche
--

INSERT INTO cities VALUES (56, 6, 'Кишинёв', false, false, 'Кишенёв', NULL, false);
INSERT INTO cities VALUES (43, 1, 'Камчатский край', false, false, 'kamchatskiy_kray', '169.782981,61.350179', false);
INSERT INTO cities VALUES (44, 1, 'Нижнекамск', false, false, 'nijnekamsk', '51.814376,55.631285', false);
INSERT INTO cities VALUES (45, 1, 'Альметьевск', false, false, 'almetevsk', '52.297113,54.901383', false);
INSERT INTO cities VALUES (46, 1, 'Архангельск', false, false, 'arhangelsk', '40.518735,64.539304', false);
INSERT INTO cities VALUES (47, 1, 'Челябинск', false, false, 'chelyabinsk', '61.400856,55.160283', false);
INSERT INTO cities VALUES (48, 1, 'Пермь', false, false, 'perm', '56.234195,58.010259', false);
INSERT INTO cities VALUES (49, 1, 'Магнитогорск', false, false, 'magnitogorsk', '58.984415,53.411677', false);
INSERT INTO cities VALUES (50, 1, 'Азнакаево', false, false, 'aznakaevo', '53.074533,54.859808', false);
INSERT INTO cities VALUES (24, 1, 'Екатеринбург', false, false, 'ekaterinburg', '60.605514,56.838607', false);
INSERT INTO cities VALUES (2, 1, 'Санкт-Петербург', true, true, 'sankt-peterburg', '30.315868,59.939095', true);
INSERT INTO cities VALUES (57, 7, 'Алма-Ата', false, false, 'Алма-Ата', NULL, false);
INSERT INTO cities VALUES (58, 1, 'Ухта', false, false, 'Ухта', NULL, false);
INSERT INTO cities VALUES (59, 1, 'Тула', false, false, 'Тула', NULL, false);
INSERT INTO cities VALUES (25, 1, 'Вологда', false, false, 'vologda', '39.891568,59.220492', false);
INSERT INTO cities VALUES (26, 1, 'Тверь', false, false, 'tver', '35.911896,56.859611', false);
INSERT INTO cities VALUES (27, 1, 'Калининград', false, false, 'kaliningrad', '20.507307,54.707390', false);
INSERT INTO cities VALUES (28, 1, 'Нижний Тагил', false, false, 'nijniy_tagil', '59.981320,57.910144', false);
INSERT INTO cities VALUES (29, 1, 'Нижний Новгород', false, false, 'nijniy_novgorod', '44.005986,56.326887', false);
INSERT INTO cities VALUES (30, 1, 'Самара', false, false, 'samara', '50.101801,53.195533', false);
INSERT INTO cities VALUES (31, 1, 'Ростов-на-Дону', false, false, 'rostov-na-donu', '39.718705,47.222531', false);
INSERT INTO cities VALUES (32, 1, 'Красноярск', false, false, 'krasnoyarsk', '92.852545,56.010569', false);
INSERT INTO cities VALUES (33, 1, 'Новосибирск', false, false, 'novosibirsk', '82.920430,55.030199', false);
INSERT INTO cities VALUES (52, 1, 'Астрахань', false, false, 'Astrahan', NULL, false);
INSERT INTO cities VALUES (53, 1, 'Якутск', false, false, NULL, NULL, false);
INSERT INTO cities VALUES (34, 1, 'Уфа', false, false, 'ufa', '55.957838,54.734768', false);
INSERT INTO cities VALUES (35, 1, 'Воронеж', false, false, 'voronej', '39.200287,51.661535', false);
INSERT INTO cities VALUES (36, 1, 'Омск', false, false, 'omsk', '73.368212,54.989342', false);
INSERT INTO cities VALUES (5, 1, 'Хабаровск', false, true, 'habarovsk', '135.057732,48.472584', false);
INSERT INTO cities VALUES (3, 2, 'Киев', false, true, 'kiev', '30.523397,50.450097', false);
INSERT INTO cities VALUES (13, 1, 'Казань', false, true, 'kazan', '49.106585,55.795793', false);
INSERT INTO cities VALUES (51, 1, 'Нерюнгри', false, false, 'neryungri', '124.711108,56.657559', false);
INSERT INTO cities VALUES (6, 1, 'Череповец', false, false, 'cherepovets', '37.906920,59.127406', false);
INSERT INTO cities VALUES (8, 1, 'Владивосток', false, false, 'vladivostok', '131.882421,43.116391', false);
INSERT INTO cities VALUES (10, 1, 'Тольятти', false, false, 'tolyatti', '49.419180,53.508714', false);
INSERT INTO cities VALUES (11, 1, 'Сургут', false, false, 'surgut', '73.396204,61.254052', false);
INSERT INTO cities VALUES (12, 1, 'Петрозаводск', false, false, 'petrozavodsk', '34.359688,61.789036', false);
INSERT INTO cities VALUES (14, 1, 'Тюмень', false, false, 'tyumen', '65.534328,57.153033', false);
INSERT INTO cities VALUES (15, 1, 'Пенза', false, false, 'penza', '45.019529,53.194546', false);
INSERT INTO cities VALUES (17, 1, 'Пятигорск', false, false, 'pyatigorsk', '43.066124,44.041163', false);
INSERT INTO cities VALUES (18, 1, 'Волгоград', false, false, 'volgograd', '44.516939,48.707103', false);
INSERT INTO cities VALUES (19, 1, 'Сочи', false, false, 'sochi', '39.722882,43.581509', false);
INSERT INTO cities VALUES (16, 1, 'Ногинск / Электросталь', false, false, 'noginsk___elektrostal', '38.444651,55.784729', false);
INSERT INTO cities VALUES (54, 1, 'Сыктывкар', false, false, 'Сыктывкар', NULL, false);
INSERT INTO cities VALUES (20, 1, 'Ярославль', false, false, 'yaroslavl', '39.893822,57.626569', false);
INSERT INTO cities VALUES (21, 1, 'Иваново', false, false, 'ivanovo', '40.973921,57.000348', false);
INSERT INTO cities VALUES (22, 1, 'Белгород', false, false, 'belgorod', '36.588849,50.597467', false);
INSERT INTO cities VALUES (23, 1, 'Набережные Челны', false, false, 'naberejnyie_chelnyi', '52.395820,55.743553', false);
INSERT INTO cities VALUES (9, 1, 'Краснодар', false, false, 'krasnodar', '38.970157,45.023877', false);
INSERT INTO cities VALUES (37, 1, 'Киров', false, false, 'kirov', '49.668077,58.602901', false);
INSERT INTO cities VALUES (38, 1, 'Кострома', false, false, 'kostroma', '40.926418,57.767683', false);
INSERT INTO cities VALUES (40, 1, 'Ишим', false, false, 'ishim', '69.479585,56.110580', false);
INSERT INTO cities VALUES (41, 1, 'Оренбург', false, false, 'orenburg', '55.096955,51.768199', false);
INSERT INTO cities VALUES (42, 1, 'Новый Уренгой', false, false, 'novyiy_urengoy', '76.680974,66.083963', false);
INSERT INTO cities VALUES (39, 1, 'Феодосия', false, false, 'feodosiya', '35.382429,45.031929', false);
INSERT INTO cities VALUES (1, 1, 'Москва', false, true, 'moskva', '37.619899,55.753676', true);
INSERT INTO cities VALUES (55, 1, 'Мурманск', false, false, 'Мурманск', NULL, false);


--
-- Name: cities_pkey; Type: CONSTRAINT; Schema: public; Owner: cardblanche; Tablespace: 
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: idx_d95db16bf92f3e70; Type: INDEX; Schema: public; Owner: cardblanche; Tablespace: 
--

CREATE INDEX idx_d95db16bf92f3e70 ON cities USING btree (country_id);


--
-- Name: uniq_d95db16b5e237e06; Type: INDEX; Schema: public; Owner: cardblanche; Tablespace: 
--

CREATE UNIQUE INDEX uniq_d95db16b5e237e06 ON cities USING btree (name);


--
-- Name: unq_country_city_name; Type: INDEX; Schema: public; Owner: cardblanche; Tablespace: 
--

CREATE UNIQUE INDEX unq_country_city_name ON cities USING btree (name, country_id);


--
-- Name: fk_d95db16bf92f3e70; Type: FK CONSTRAINT; Schema: public; Owner: cardblanche
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT fk_d95db16bf92f3e70 FOREIGN KEY (country_id) REFERENCES countries(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

