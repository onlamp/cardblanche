<?php

namespace Acme\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Acme\FrontBundle\Entity\Offer;
/**
* @Route("/" )
*/
class DefaultController extends Controller
{

    /**
     * @Route("/header/{city_id}", name="header_part")
     * @Template()
     */
    public function headerAction($city_id)
    {   
        $conn = $this->get('database_connection');
        $sSqlSelect = "SELECT * FROM cities";
        $aCities = $conn->fetchAll($sSqlSelect);
        return array( 'cities'=>$aCities, 'city_id'=>$city_id );
    }  

    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function indexAction()
    {
        return new RedirectResponse($this->generateUrl('offers', array('sCategorySlug'=>'', 'iPageNumber'=>'1')));
    }   
    
    // нужен
    /**
     * @Route("/price", name="front_index_page" )
     * @Template()
     *
    */
    public function priceAction(Request $request)
    {
    	// * @Secure(roles="ROLE_REGION")
        $conn = $this->get('database_connection');
        // list of tarifs
        $sqlTariffs = "SELECT id, name, cost, hightlight, sort_order FROM tariffs WHERE id>0 AND id<5 ORDER BY sort_order LIMIT 4 ";
        $aTarifs = $conn->fetchAll($sqlTariffs);
        if( count($aTarifs) != 4){
            throw new \Exception("Error: You must have four active tariffs", 1);
        }
        
        $num_col_1 = $aTarifs[0]['id'];
        $num_col_2 = $aTarifs[1]['id'];
        $num_col_3 = $aTarifs[2]['id'];
        $num_col_4 = $aTarifs[3]['id'];
        
        // option of each tarif
        $sqlSelect = "SELECT id, o_name, o_type, o_hint, o_city_id, sort_order, hightlight, value_$num_col_1 AS value_1, value_$num_col_2 AS value_2, value_$num_col_3 AS value_3, value_$num_col_4 AS value_4 FROM tarif_option WHERE enabled=1  ORDER BY sort_order";

        $aTarifOptions = $conn->fetchAll( $sqlSelect );
        return array( "aTarifs" => $aTarifs, "aTarifOptions" => $aTarifOptions );
    }


    /**
     * @Route("/price/check.{_format}", name="front_price_check" )
     * @Template()
     */
    public function priceCheckAction(Request $request)
    {
        $method = 'get';
        if($request->isMethod('POST')){
            $method = 'POST';
            $data = $request->request->get('data');
        }
        if($request->isMethod('GET')){
            $method = 'GET';
            $data = $request->query->get('data');
        }
        return array('result' => array($data, $method) );
    }

    /**
     * @Route("/price/callcheck.{_format}", name="front_price_call_check" )
     * @Template()
     */
    public function callpriceCheckAction(Request $request)
    {
        if($request->isMethod('POST')){
            $data = $request->request->get('data');
        }
        if($request->isMethod('GET')){
            $data = $request->query->get('data');
        }
        return $this->forward("AcmeFrontBundle:Default:priceCheck", array(), array('data'=>$data) );
        //return array('result' => $data );
    }





}
