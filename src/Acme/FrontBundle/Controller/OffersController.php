<?php

namespace Acme\FrontBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
* @Route("/{_locale}", defaults={"_locale"="ru"})
*/
class OffersController extends Controller
{
    private $iCityId = 1; //moscow as default city
    private $iItemsOnPage = 12;
    
    // this func check and session storage system and return sCitySlug
    // when iCityId is CITY_ID then force session storage city 
    private function getCitySlug( $iCityId = null ){
        $request = $this->getRequest();
        $session = $request->getSession();
    }

    /**
     * @Route("/{sCitySlug}/offer/list/no-found/{sCategorySlug}", defaults={"sCitySlug"="moskva", "sCategorySlug"="" }, name="offers_no_found")
     * @Template()
     */
    public function noFoundOffersAction($sCitySlug, $sCategorySlug)
    {
        //@todo - if session store CITY_ID "not equal" sCitySlug
        //@todo - than need for goole/yandex crawler our site
        //@todo - refresh session value in cities is different 
        return array(
            'locale' => $this->get('request')->getLocale(),
            'sCategorySlug' => $sCategorySlug,
            'sCitySlug' => $sCitySlug
        );
    }      

    /**
     * @Route("/{sCitySlug}/offer/list/{sCategorySlug}/{iPageNumber}", requirements={"iPageNumber" = "\d+"}, defaults={"sCitySlug"="moskva", "sCategorySlug"= "", "iPageNumber" = 1}, name="offers")
     * @Template()
     */
    public function offersAction($sCitySlug, $sCategorySlug, $iPageNumber)
    {
        if (is_numeric($sCategorySlug) && $iPageNumber==1) {
            $iPageNumber=$sCategorySlug;
            $sCategorySlug='';
        }
        
        $conn = $this->get('database_connection');
        $sqlSelect = "SELECT COUNT(*) FROM offers o INNER JOIN cities c ON o.city_id = c.id INNER JOIN offers_rubrics_list orl ON o.id = orl.offer_id INNER JOIN offers_rubrics orb ON orl.offerrubric_id = orb.id WHERE o.public = TRUE and o.actual_till > NOW() and c.seo_name LIKE '$sCitySlug' and orb.url LIKE '%$sCategorySlug%'";
        $iTotalItems = $conn->fetchColumn($sqlSelect);
        if (!($iTotalItems>0)){
            return new RedirectResponse($this->generateUrl('offers_no_found', array('sCitySlug'=>$sCitySlug, 'sCategorySlug'=>$sCategorySlug)));
        }
        
        $sqlSelect = "SELECT o.*, orb.name as category_name FROM offers o INNER JOIN cities c ON o.city_id = c.id INNER JOIN offers_rubrics_list orl ON o.id = orl.offer_id INNER JOIN offers_rubrics orb ON orl.offerrubric_id = orb.id WHERE o.public = TRUE and o.actual_till > NOW() and c.seo_name LIKE '$sCitySlug' and orb.url LIKE '%$sCategorySlug%' LIMIT $this->iItemsOnPage OFFSET $this->iItemsOnPage*($iPageNumber-1)";
        $aOffers = $conn->fetchAll($sqlSelect);
        return array(
            'offers' => $aOffers, 
            'pagination'=>(object) array(
                'currentPage' => $iPageNumber,
                'totalPage' => ceil($iTotalItems/$this->iItemsOnPage)       
            ),
            'locale' => $this->get('request')->getLocale(),
            'sCategorySlug' => $sCategorySlug,
            'sTitle' => ($sCategorySlug!='')?$aOffers[0]['category_name']:'Все категории',
            'sCitySlug' => $sCitySlug,
            'json' => json_encode($aOffers)
        );
    }   
    /**
     * @Route("/{sCitySlug}/offer/{sCategorySlug}/{iOfferId}", defaults={"sCitySlug"="moskva", "sCategorySlug"= "", "iOfferId"=0}, name="offer")
     * @Template()
     */
    public function offerAction($sCitySlug, $sCategorySlug, $iOfferId)
    {
        if (is_numeric($sCategorySlug)) {
            $iOfferId=$sCategorySlug;
            $sCategorySlug='';
        }        
        if (!is_numeric($iOfferId))
            throw $this->createNotFoundException('The offer does not exist');
        $conn = $this->get('database_connection');
        $aOffer = $conn->fetchAssoc("SELECT o.* FROM offers o INNER JOIN cities c ON o.city_id = c.id INNER JOIN offers_rubrics_list orl ON o.id = orl.offer_id INNER JOIN offers_rubrics orb ON orl.offerrubric_id = orb.id WHERE o.id='$iOfferId' and o.public = TRUE and o.actual_till > NOW() and c.seo_name LIKE '$sCitySlug' and orb.url LIKE '%$sCategorySlug%' LIMIT 1");
        if (!$aOffer)
            throw $this->createNotFoundException('The offer does not exist');
        return array(
            'locale' => $this->get('request')->getLocale(),
            'offer' => $aOffer,
            'sCategorySlug' => $sCategorySlug,
            'sCitySlug' => $sCitySlug
        );
    }        
}
