<?php

namespace Acme\FrontBundle\Form;
// src/MyCompany/MyBundle/Form/CreateAccountForm.php
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateAccountForm extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        switch ( $options['flow_step'] ){
        
            case 1:

                $builder
                        ->add('card_number', 'text', array(
                            'required' => false,
                            'label' => 'Номер Вашей карты',
                            'attr' => array('class' => 'span4') ))

                        ->add('head_user_id', 'hidden', array(
                            'required' => false,
                        ))

                        ->add('card_id', 'text', array(
                            'required' => false,
                            'label' => 'ID на Вашей карте',
                            'attr' => array('class' => 'span4') ))
/*
                        ->add('country', 'entity', array(
                            'label' => 'Страна',
                            'class' => 'AcmeBaseBundle:Countries',
                            'attr' => array('class' => 'span4'),
                            'empty_value' => 'Выбор страны',
                        ))
*/
                        ->add('city', 'entity', array(
                            'label' => 'Город',
                            'class' => 'AcmeRegionBundle:Cities',
                            'attr' => array('class' => 'span4'),
                            'empty_value' => 'Выбор города',
                        ));              
                break;        

            case 2:
                if( $options['data']->card_number && $options['data']->card_id ){
// add validate this values (as variant) 
                }
                $builder->add('tarif', 'choice', array(
                    'choices' => array('1' => 'Classic', '2' => 'Premium', '3' => 'Business', '4' => 'Trial'),
                    'preferred_choices' => array('1'),
                    'expanded' => true,
                    'label' => 'Доступные тарифы',
                    'attr' => array('class' => 'hiden_list'),
                    'required' => false,
                    'empty_data' => '1',
                ));

                break;

            case 3:
                if( $options['data']->card_number && $options['data']->card_id && $options['data']->head_user_id ){

                        $builder->add('phone', 'text', array(
                            'label' => 'Телефон (без 8, только 10 цифр)', 
                            'required' => true,
                            'attr' => array('class' => 'span4') ))
                        
                        ->add('email', 'email', array(
                            'label'=>'Ваш email', 
                            'required' => true,
                            'attr' => array('class' => 'span4') ))
                        
                        ->add('referal_user_id', 'hidden', array(
                            'label'=>'ID пригласившего', 
                            'required' => true,    
                            'attr' => array('class' => 'span4', 'value'=>$options['data']->head_user_id) ))
                        
                        ->add('subscribe_email', 'choice', array(
                            'label'=>'Новости на почту',
                            'required' => true,
                            'attr' => array('class' => 'span4'),
                            'choices'=>array('1'=>'Присылать мне уведомления', '0'=>'Не присылать'),
                            'empty_value'=>'Присылать новости?' ))
                        ->add('subscribe_sms', 'choice', array('label'=>'Новости на SMS',
                            'attr' => array('class' => 'span4'),
                            'choices'=>array('1'=>'Получать', '0'=>'Не получать'),
                            'empty_value'=>'Получать SMS с акциями?' ));       
                } else {
                // Clean user
                    $builder->add('phone', 'text', array(
                            'label' => 'Телефон (без 8, только 10 цифр)', 
                            'required' => true,
                            'attr' => array('class' => 'span4') ))
                		
                        ->add('email', 'text', array(
                            'label'=>'Ваш email', 
                            'required' => true,
                            'attr' => array('class' => 'span4') ))
                		
                        ->add('referal_user_id', 'text', array(
                            'label'=>'ID пригласившего', 
                            'required' => true,    
                            'attr' => array('class' => 'span4') ))
                		
                        ->add('subscribe_email', 'choice', array(
                            'label'=>'Новости на почту',
                            'required' => true,
                            'attr' => array('class' => 'span4'),
                			'choices'=>array('1'=>'Присылать мне уведомления', '0'=>'Не присылать'),
                			'empty_value'=>'Присылать новости?'	)) 
                        ->add('subscribe_sms', 'choice', array('label'=>'Новости на SMS',
                            'attr' => array('class' => 'span4'),
                            'choices'=>array('1'=>'Получать', '0'=>'Не получать'),
                            'empty_value'=>'Получать SMS с акциями?' ));
                }
                break;

            case 4:
                $builder->add('accept', 'checkbox', array(
                    'label' => 'Я понимаю эти документы и принимаю условия и положения, изложенные в полном объеме, без оговорок и условий.',
                    //'attr' => array('class' => 'span9'),
                    'required' => true,
                ) );
                break;

                /*
            case 2:
                $builder->add('engine', 'form_type_vehicleEngine', array(
                    'empty_value' => '',
                ));
                break;
                */
        }
    }

    public function getName() {
        return 'createAccount';
    }

}
