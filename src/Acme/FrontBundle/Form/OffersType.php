<?php

namespace Acme\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OffersType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('actualTill')
            ->add('price')
            ->add('discountPrice')
            ->add('description')
            ->add('image')
            ->add('createdat')
            ->add('public')
            ->add('onsmartbanner')
            ->add('smartbannerorder')
            ->add('toNewsletter')
            ->add('count')
            ->add('sticky')
            ->add('cardblanchemanageremail')
            ->add('partnermanageremail')
            ->add('manager')
            ->add('descmobile')
            ->add('address')
            ->add('position')
            ->add('deleted')
            ->add('rawdescription')
            ->add('offercost')
            ->add('howtoget')
            ->add('type')
            ->add('latitude')
            ->add('longitude')
            ->add('city')
            ->add('partner')
            ->add('offerrubric')
            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\FrontBundle\Entity\Offers'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'acme_frontbundle_offers';
    }
}
