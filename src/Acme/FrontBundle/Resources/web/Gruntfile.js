module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        bowercopy: {
            options: {
                srcPrefix: ''
            },            
            css: {
		options: {
		    destPrefix: '../../../../../web/bs3/css/'
		},
                files: {
                    'bootstrap.css': 'bower_components/bootstrap/dist/css/bootstrap.css',
                    'bootstrap.css.map': 'bower_components/bootstrap/dist/css/bootstrap.css.map',
                    'bootstrap-theme.css': 'css/bootstrap-theme.css',
                    'font-awesome.css': 'bower_components/font-awesome/css/font-awesome.css',
                    'style.css': 'css/style.css'
                }                
            },
            js: {
		options: {
		    destPrefix: '../../../../../web/bs3/js/'
		},
                files: {
                    'jquery.js': 'bower_components/jquery/dist/jquery.js',
                    'jquery-ui.js': 'bower_components/jquery-ui/jquery-ui.js',
                    'bootstrap.js': 'bower_components/bootstrap/dist/js/bootstrap.js'
                }                
            },        
            font: {
		options: {
		    destPrefix: '../../../../../web/bs3/'
		},
                files: {
                    '': 'bower_components/**/fonts'
                }
            }   
        }
    });
    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.registerTask('default', ['bowercopy']);
};