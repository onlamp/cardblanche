<?php

namespace Acme\FrontBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PartialControllerTest extends WebTestCase
{
    public function testUpmenu()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/partial/upmenu');
    }

    public function testDownmenu()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/partial/downmenu');
    }

    public function testFooter()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/partial/footer');
    }

}
