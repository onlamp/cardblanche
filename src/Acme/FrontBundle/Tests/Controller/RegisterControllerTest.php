<?php

namespace Acme\FrontBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegisterControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/register/index');
    }

    public function testError()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/register/action');
    }

    public function testForgot()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/register/forgot');
    }

    public function testSuccess()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/register/success');
    }

}
