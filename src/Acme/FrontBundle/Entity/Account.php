<?php

namespace Acme\FrontBundle\Entity;

use Symfony\Component\Validator\Constraints AS Assert;
use Symfony\Component\Validator\Constraints\EmailValidator;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\InjectParams;
use JMS\DiExtraBundle\Annotation\Service;


class Account{

    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
        //$this->session = $session;
    }

	public $country;
	public $head_user_id;

	public $card_number;
	

	public $card_id;
	public $city;

	/**
	 * @Assert\NotBlank(groups={"flow_createAccount_step2"})
     */
	public $tarif;	
	

	public $username;

	/**
     * @Assert\Length(
     *      min = 10,
     *      max = 10,
     *      minMessage = "должен содержать не меньше {{ limit }} символов",
     *      maxMessage = "должен содержать не больше {{ limit }} символов",
     *      exactMessage = "должен содержать точно {{ limit }} символов",
     *      groups={"flow_createAccount_step3"}
     * )
     */
	public $phone;

    /**
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
	public $email;
	
	/**
	 * @Assert\NotBlank(groups={"flow_createAccount_step3"})
     */
	public $referal_user_id;
	
	public $subscribe_email;
	
	public $subscribe_sms;

	/**
	 * @Assert\NotBlank(groups={"flow_createAccount_step4"})
     */
	public $accept;

	/**
	 * @Assert\True(message="Номер телефона это просто 10 цифр", groups={"flow_createAccount_step3"} )
	 */
	public function isPhoneNumberRight(){
		$phone = '';
		if( strlen($this->phone)==10 && preg_match('|[0-9]{10}|', $this->phone ) ){
			// @todo: correct contrycode
			//$this->phone = '+7' . $this->phone;
			// add country code when send register
			return true;
		}
		return false;
	}

	/**
	 * @Assert\True(message="Этот телефон уже зарегистрирован", groups={"flow_createAccount_step3"} )
	 */
	public function isPhoneExist(){
		if( strlen($this->phone)==10 && preg_match('|[0-9]{10}|', $this->phone ) ){
			$phone = $this->phone;
			$aPhone = $this->conn->fetchArray( "SELECT * FROM users WHERE phone LIKE '%$phone%' " );
			//var_dump($aPhone);
			if( !$aPhone ){
				return true;
			}
		}
		return false;
	}


    /**
     * @Assert\True(message = "Ошибка: Данные пластиковой карты недействительны! У вас осталось 3 попытки для правильного указания данных. Если укажете неверно, придется подождать 15 минут.", groups={"flow_createAccount_step1"})
	 *
	 */
	public function isCardNumberCardIdLegal(){
		$result = false;
		if($this->card_number == null && $this->card_id == null){
			return true;
		} 
		// check user input data
		$cardNumber = $this->card_number;
		$cardId = $this->card_id;
		$sqlSelect = "SELECT * FROM cardblanchecard WHERE id=? AND user_id=? ";
		$aCardblancheCard = $this->conn->fetchAll( $sqlSelect, array( $cardNumber, $cardId ) );
		if( count($aCardblancheCard) == 1 && $aCardblancheCard[0]['id'] == $cardNumber && $aCardblancheCard[0]['user_id'] == $cardId ){
			$aHeadUser = $this->conn->fetchArray( "SELECT id, referal_user_id AS ruid FROM users WHERE id=?", array($cardId) );
			//@todo: Do nothing! In this card data not valid - user NOT ACTIVATE

			//@todo: If this card is ACTIVE - do nothing! 
			$this->head_user_id = $aHeadUser[1];
			return true;
		}
		return $result;
	}

	public function canHaveCountry( ){
		return true;
	}

	public function canHaveCity( ){
		return true;
	}

	public function canHaveTarif( ){
		return true;
	}

	public function canHaveReferalUserId( ){
		return true;
	}

	public function canHaveAccept( ){
		return true;
	}

}




/**
 * Assert\NotBlank(groups={"flow_createAccount_step1"})
 */



