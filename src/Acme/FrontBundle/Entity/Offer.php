<?php
namespace Acme\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="offers")
 */
class Offer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id; 
    
   /**
     * @ORM\Column(type="integer")
     */
    public $partner_id;
    
   /**
     * @ORM\Column(type="integer")
     */
    public $city_id;
    
   /**
     * @ORM\Column(type="string")
     */
    public $title;
    
   /**
     * @ORM\Column(type="datetime")
     */
    public $actual_till;
    
   /**
     * @ORM\Column(type="float")
     */
    public $price;
    
   /**
     * @ORM\Column(type="float")
     */
    public $discount_price;
    
   /**
     * @ORM\Column(type="text")
     */
    public $description;
    
   /**
     * @ORM\Column(type="string")
     */
    public $image;
    
   /**
     * @ORM\Column(type="datetime")
     */
    public $createdat;
    
   /**
     * @ORM\Column(type="boolean")
     */
    public $public;
    
   /**
     * @ORM\Column(type="boolean")
     */
    public $onsmartbanner;
    
   /**
     * @ORM\Column(type="integer")
     */
    public $smartbannerorder;
    
   /**
     * @ORM\Column(type="boolean")
     */
    public $to_newsletter;
    
   /**
     * @ORM\Column(type="integer")
     */
    public $count;
    
   /**
     * @ORM\Column(type="boolean")
     */
    public $sticky;
    
   /**
     * @ORM\Column(type="string")
     */
    public $cardblanchemanageremail;
    
   /**
     * @ORM\Column(type="string")
     */
    public $partnermanageremail;
    
   /**
     * @ORM\Column(type="string")
     */
    public $manager;
    
   /**
     * @ORM\Column(type="text")
     */
    public $descmobile;
    
   /**
     * @ORM\Column(type="string")
     */
    public $address;  
    
}