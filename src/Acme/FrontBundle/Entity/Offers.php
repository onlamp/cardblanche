<?php

namespace Acme\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offers
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Acme\FrontBundle\Entity\OffersRepository")
 */
class Offers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

/**************************************/
 /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actual_till", type="datetime", nullable=false)
     */
    private $actualTill;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $discountPrice;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdat", type="datetime", nullable=false)
     */
    private $createdat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="public", type="boolean", nullable=true)
     */
    private $public;

    /**
     * @var boolean
     *
     * @ORM\Column(name="onsmartbanner", type="boolean", nullable=true)
     */
    private $onsmartbanner;

    /**
     * @var integer
     *
     * @ORM\Column(name="smartbannerorder", type="integer", nullable=true)
     */
    private $smartbannerorder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="to_newsletter", type="boolean", nullable=false)
     */
    private $toNewsletter = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", nullable=true)
     */
    private $count;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sticky", type="boolean", nullable=false)
     */
    private $sticky = false;

    /**
     * @var string
     *
     * @ORM\Column(name="cardblanchemanageremail", type="string", length=255, nullable=true)
     */
    private $cardblanchemanageremail;

    /**
     * @var string
     *
     * @ORM\Column(name="partnermanageremail", type="string", length=255, nullable=true)
     */
    private $partnermanageremail;

    /**
     * @var string
     *
     * @ORM\Column(name="manager", type="string", length=255, nullable=true)
     */
    private $manager;

    /**
     * @var string
     *
     * @ORM\Column(name="descmobile", type="text", nullable=true)
     */
    private $descmobile;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted = false;

    /**
     * @var string
     *
     * @ORM\Column(name="rawdescription", type="text", nullable=true)
     */
    private $rawdescription;

    /**
     * @var float
     *
     * @ORM\Column(name="offercost", type="float", precision=10, scale=0, nullable=true)
     */
    private $offercost;

    /**
     * @var string
     *
     * @ORM\Column(name="howtoget", type="text", nullable=true)
     */
    private $howtoget;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=true)
     */
    private $longitude;

    /**
     * @var \Cities
     *
     * @ORM\ManyToOne(targetEntity="Cities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \Partners
     *
     * @ORM\ManyToOne(targetEntity="Partners")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="partner_id", referencedColumnName="id")
     * })
     */
    private $partner;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Users", inversedBy="offer")
     * @ORM\JoinTable(name="offer_send_user",
     *   joinColumns={
     *     @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *   }
     * )
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="OffersRubrics", inversedBy="offer")
     * @ORM\JoinTable(name="offers_rubrics_list",
     *   joinColumns={
     *     @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="offerrubric_id", referencedColumnName="id")
     *   }
     * )
     */
    private $offerrubric;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offerrubric = new \Doctrine\Common\Collections\ArrayCollection();
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Offers
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set actualTill
     *
     * @param \DateTime $actualTill
     * @return Offers
     */
    public function setActualTill($actualTill)
    {
        $this->actualTill = $actualTill;

        return $this;
    }

    /**
     * Get actualTill
     *
     * @return \DateTime 
     */
    public function getActualTill()
    {
        return $this->actualTill;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Offers
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set discountPrice
     *
     * @param float $discountPrice
     * @return Offers
     */
    public function setDiscountPrice($discountPrice)
    {
        $this->discountPrice = $discountPrice;

        return $this;
    }

    /**
     * Get discountPrice
     *
     * @return float 
     */
    public function getDiscountPrice()
    {
        return $this->discountPrice;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Offers
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Offers
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set createdat
     *
     * @param \DateTime $createdat
     * @return Offers
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

    /**
     * Get createdat
     *
     * @return \DateTime 
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

    /**
     * Set public
     *
     * @param boolean $public
     * @return Offers
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean 
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set onsmartbanner
     *
     * @param boolean $onsmartbanner
     * @return Offers
     */
    public function setOnsmartbanner($onsmartbanner)
    {
        $this->onsmartbanner = $onsmartbanner;

        return $this;
    }

    /**
     * Get onsmartbanner
     *
     * @return boolean 
     */
    public function getOnsmartbanner()
    {
        return $this->onsmartbanner;
    }

    /**
     * Set smartbannerorder
     *
     * @param integer $smartbannerorder
     * @return Offers
     */
    public function setSmartbannerorder($smartbannerorder)
    {
        $this->smartbannerorder = $smartbannerorder;

        return $this;
    }

    /**
     * Get smartbannerorder
     *
     * @return integer 
     */
    public function getSmartbannerorder()
    {
        return $this->smartbannerorder;
    }

    /**
     * Set toNewsletter
     *
     * @param boolean $toNewsletter
     * @return Offers
     */
    public function setToNewsletter($toNewsletter)
    {
        $this->toNewsletter = $toNewsletter;

        return $this;
    }

    /**
     * Get toNewsletter
     *
     * @return boolean 
     */
    public function getToNewsletter()
    {
        return $this->toNewsletter;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return Offers
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set sticky
     *
     * @param boolean $sticky
     * @return Offers
     */
    public function setSticky($sticky)
    {
        $this->sticky = $sticky;

        return $this;
    }

    /**
     * Get sticky
     *
     * @return boolean 
     */
    public function getSticky()
    {
        return $this->sticky;
    }

    /**
     * Set cardblanchemanageremail
     *
     * @param string $cardblanchemanageremail
     * @return Offers
     */
    public function setCardblanchemanageremail($cardblanchemanageremail)
    {
        $this->cardblanchemanageremail = $cardblanchemanageremail;

        return $this;
    }

    /**
     * Get cardblanchemanageremail
     *
     * @return string 
     */
    public function getCardblanchemanageremail()
    {
        return $this->cardblanchemanageremail;
    }

    /**
     * Set partnermanageremail
     *
     * @param string $partnermanageremail
     * @return Offers
     */
    public function setPartnermanageremail($partnermanageremail)
    {
        $this->partnermanageremail = $partnermanageremail;

        return $this;
    }

    /**
     * Get partnermanageremail
     *
     * @return string 
     */
    public function getPartnermanageremail()
    {
        return $this->partnermanageremail;
    }

    /**
     * Set manager
     *
     * @param string $manager
     * @return Offers
     */
    public function setManager($manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return string 
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set descmobile
     *
     * @param string $descmobile
     * @return Offers
     */
    public function setDescmobile($descmobile)
    {
        $this->descmobile = $descmobile;

        return $this;
    }

    /**
     * Get descmobile
     *
     * @return string 
     */
    public function getDescmobile()
    {
        return $this->descmobile;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Offers
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Offers
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Offers
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set rawdescription
     *
     * @param string $rawdescription
     * @return Offers
     */
    public function setRawdescription($rawdescription)
    {
        $this->rawdescription = $rawdescription;

        return $this;
    }

    /**
     * Get rawdescription
     *
     * @return string 
     */
    public function getRawdescription()
    {
        return $this->rawdescription;
    }

    /**
     * Set offercost
     *
     * @param float $offercost
     * @return Offers
     */
    public function setOffercost($offercost)
    {
        $this->offercost = $offercost;

        return $this;
    }

    /**
     * Get offercost
     *
     * @return float 
     */
    public function getOffercost()
    {
        return $this->offercost;
    }

    /**
     * Set howtoget
     *
     * @param string $howtoget
     * @return Offers
     */
    public function setHowtoget($howtoget)
    {
        $this->howtoget = $howtoget;

        return $this;
    }

    /**
     * Get howtoget
     *
     * @return string 
     */
    public function getHowtoget()
    {
        return $this->howtoget;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Offers
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Offers
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Offers
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set city
     *
     * @param \Acme\FrontBundle\Entity\Cities $city
     * @return Offers
     */
    public function setCity(\Acme\FrontBundle\Entity\Cities $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Acme\FrontBundle\Entity\Cities 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set partner
     *
     * @param \Acme\FrontBundle\Entity\Partners $partner
     * @return Offers
     */
    public function setPartner(\Acme\FrontBundle\Entity\Partners $partner = null)
    {
        $this->partner = $partner;

        return $this;
    }

    /**
     * Get partner
     *
     * @return \Acme\FrontBundle\Entity\Partners 
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Add offerrubric
     *
     * @param \Acme\FrontBundle\Entity\OffersRubrics $offerrubric
     * @return Offers
     */
    public function addOfferrubric(\Acme\FrontBundle\Entity\OffersRubrics $offerrubric)
    {
        $this->offerrubric[] = $offerrubric;

        return $this;
    }

    /**
     * Remove offerrubric
     *
     * @param \Acme\FrontBundle\Entity\OffersRubrics $offerrubric
     */
    public function removeOfferrubric(\Acme\FrontBundle\Entity\OffersRubrics $offerrubric)
    {
        $this->offerrubric->removeElement($offerrubric);
    }

    /**
     * Get offerrubric
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOfferrubric()
    {
        return $this->offerrubric;
    }

    /**
     * Add user
     *
     * @param \Acme\FrontBundle\Entity\Users $user
     * @return Offers
     */
    public function addUser(\Acme\FrontBundle\Entity\Users $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Acme\FrontBundle\Entity\Users $user
     */
    public function removeUser(\Acme\FrontBundle\Entity\Users $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }
}
