<?php

namespace Acme\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OffersRubrics
 *
 * @ORM\Table(name="offers_rubrics", uniqueConstraints={@ORM\UniqueConstraint(name="uniq_369a06d1f47645ae", columns={"url"}), @ORM\UniqueConstraint(name="uniq_369a06d15e237e06", columns={"name"})})
 * @ORM\Entity
 */
class OffersRubrics
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="offers_rubrics_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Offers", mappedBy="offerrubric")
     */
    private $offer;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Partners", mappedBy="offerrubric")
     */
    private $partner;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="PartnersInfo", mappedBy="offerrubric")
     */
    private $partnerinfo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Quiz", mappedBy="offerrubric")
     */
    private $quiz;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->offer = new \Doctrine\Common\Collections\ArrayCollection();
        $this->partner = new \Doctrine\Common\Collections\ArrayCollection();
        $this->partnerinfo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->quiz = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
