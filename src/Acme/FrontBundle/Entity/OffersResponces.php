<?php

namespace Acme\FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OffersResponces
 *
 * @ORM\Table(name="offers_responces")
 * @ORM\Entity
 */
class OffersResponces
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="offers_responces_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="offer_id", type="integer", nullable=true)
     */
    private $offerId;

    /**
     * @var string
     *
     * @ORM\Column(name="responce", type="text", nullable=true)
     */
    private $responce;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="text", nullable=true)
     */
    private $answer;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_published", type="integer", nullable=false)
     */
    private $isPublished = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="date", nullable=true)
     */
    private $createdAt;


}
